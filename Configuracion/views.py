from django.shortcuts import render

def inicio(request):
    return render(request, 'inicio.html')

def lista_productos(request):
    return render(request, 'lista_productos.html')

def lista_clientes(request):
    return render(request, 'lista_clientes.html')

def estudio_envios(request):
    return render(request, 'estudio_envios.html')

def mapa_clientes(request):
    return render(request, 'mapa_clientes.html')

def informacion(request):
    return render(request, 'informacion.html')

def inteligencia_artificial(request):
    return render(request, 'inteligencia_artificial.html')
