from django.contrib import admin
from django.urls import path
from Configuracion import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.inicio, name='inicio'),
    path('productos/', views.lista_productos, name='lista_productos'),
    path('clientes/', views.lista_clientes, name='lista_clientes'),
    path('envios/', views.estudio_envios, name='estudio_envios'),
    path('mapa/', views.mapa_clientes, name='mapa_clientes'),
    path('informacion/', views.informacion, name='informacion'),
    path('ia/', views.inteligencia_artificial, name='inteligencia_artificial'),
]
